<?php

namespace App\Traits;

use Carbon\Carbon;
use Illuminate\Http\Request;

trait CarsBookingTrait
{
    public function carsbooking($rc_cars, $start, $end)
    {
        $results = [];
        foreach ($rc_cars as $rc_car) {
            //$bookingCount = $rc_car->rc_bookings->count();
            //dd($bookingCount);

            //$stats = $rc_car->calculateFreeDays($start, $end);
            //$rc_car->free_days = $stats['free_days'];
            //$rc_car->total_days_in_month = $stats['total_days_in_month'];
            // Обчислюємо кількість вільних годин в періоді
            $freeHours = 0;

            $bookings = $rc_car->rc_bookings;
            foreach ($bookings as $booking) {
                // Перевіряємо, чи бронювання знаходиться в періоді
                $bookingStart = Carbon::parse($booking->start_date);
                $bookingEnd = Carbon::parse($booking->end_date);

                if ($bookingStart >= $start && $bookingEnd <= $end) {
                    // Визначаємо тривалість бронювання в годинах
                    $durationHours = $bookingStart->diffInHours($bookingEnd);

                    // Додаємо тривалість бронювання до загальної кількості годин
                    $freeHours += $durationHours;
                }
            }

            // Обчислюємо кількість вільних днів
            $totalDays = $end->diffInDays($start) + 1; // +1 для включення обох кінців
            $freeDays = $totalDays - ($freeHours >= 9 * ($end->diffInDays($start) + 1) ? 1 : 0); // Перевірка на кількість вільних годин

            //$rc_car->free_days = $freeDays;
            //$rc_car->totalDays = $totalDays;


            // Додаємо результат до масиву
            $results[] = [
                'car_id' => $rc_car->car_id,
                'car_name' => $rc_car->rc_cars_translations->title,
                'free_days' => $freeDays,
                'total_days' => $totalDays,
            ];
        }
        return  $results;
        /*$locales = RcCarsBrandsTranslation::distinct()->pluck('lang')->toArray();
        if (!in_array($lang, $locales)) {
            $lang = 'en';
        }
        app()->setLocale($lang);

        //return $lang;
        return response()->json([
            'lang' => $lang,
            'locales' => $locales,
        ], 200);*/
    }
}
