<?php

namespace App\Traits;

use App\Models\CarsBrands\RcCarsBrandsTranslation;

trait LocalizationTrait
{
    public function checkLocalization($lang)
    {
        $locales = RcCarsBrandsTranslation::distinct()->pluck('lang')->toArray();
        if (!in_array($lang, $locales)) {
            $lang = 'en';
        }
        app()->setLocale($lang);

        return response()->json([
            'lang' => $lang,
            'locales' => $locales,
        ], 200);
    }
}
