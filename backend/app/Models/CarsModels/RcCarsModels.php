<?php

namespace App\Models\CarsModels;

use App\Models\Cars\RcCars;
use App\Models\CarsBrands\RcCarsBrands;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RcCarsModels extends Model
{
    use HasFactory;

    public function rc_cars_models_translations()
    {
        return $this->hasOne(RcCarsModelsTranslation::class, 'car_model_id', 'car_model_id')
            ->where([
                'lang' => app()->getLocale()
            ]);
    }

    public function rc_cars()
    {
        return $this
            ->hasMany(RcCars::class, 'car_model_id', 'car_model_id');
    }

    public function rc_cars_brands()
    {
        return $this
            ->hasOne(RcCarsBrands::class, 'car_brand_id', 'car_brand_id');
    }
}
