<?php

namespace App\Models\CarsModels;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RcCarsModelsTranslation extends Model
{
    use HasFactory;
}
