<?php

namespace App\Models\Bookings;

use App\Models\Cars\RcCars;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RcBookings extends Model
{
    use HasFactory;

    public function rc_cars()
    {
        return $this
            ->hasMany(RcCars::class, 'car_id', 'car_id');
    }
}
