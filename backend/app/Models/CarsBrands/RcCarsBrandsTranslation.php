<?php

namespace App\Models\CarsBrands;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RcCarsBrandsTranslation extends Model
{
    use HasFactory;

    public function carBrand()
    {
        return $this->belongsTo(RcCarsBrands::class, 'car_brand_id', 'car_brand_id');
    }
}
