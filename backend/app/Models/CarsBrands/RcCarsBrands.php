<?php

namespace App\Models\CarsBrands;

use App\Models\Cars\RcCars;
use App\Models\CarsModels\RcCarsModels;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RcCarsBrands extends Model
{
    use HasFactory;

    public function rc_cars_brands_translations()
    {
        return $this
            ->hasOne(RcCarsBrandsTranslation::class, 'car_brand_id', 'car_brand_id')
            ->where([
                'lang' => app()->getLocale()
                ]);
    }

    public function rc_cars_models()
    {
        return $this
            ->hasMany(RcCarsModels::class, 'car_brand_id', 'car_brand_id');
    }
    public function rc_cars()
    {
        return $this
            ->hasMany(RcCars::class, 'car_model_id', 'car_model_id');
    }
}
