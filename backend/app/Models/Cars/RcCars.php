<?php

namespace App\Models\Cars;

use App\Models\Bookings\RcBookings;
use App\Models\CarsBrands\RcCarsBrands;
use App\Models\CarsModels\RcCarsModels;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RcCars extends Model
{
    use HasFactory;


    public function rc_cars_translations()
    {
        return $this
            ->hasOne(RcCarsTranslation::class, 'car_id', 'car_id')
            ->where([
                'lang' => app()->getLocale()
            ]);
    }

    public function rc_cars_models()
    {
        return $this
            ->hasOne(RcCarsModels::class, 'car_model_id', 'car_model_id')
            ->with('rc_cars_brands');
    }

    public function rc_cars_brands()
    {
        return $this
            ->hasOne(RcCarsBrands::class, 'car_brand_id', 'car_brand_id');
    }

    public function rc_bookings()
    {
        $currentDate = Carbon::now();
        $startOfMonth = $currentDate->startOfMonth();
        $endOfMonth = $currentDate->endOfMonth();

        return $this
            ->hasMany(RcBookings::class, 'car_id', 'car_id')
            ->where([
                'status' => 1,
                'company_id' => 1,
                'is_deleted' => !1,
            ]);/*->selectRaw('*,
            TIMESTAMPDIFF(SECOND, start_date, end_date) / 3600 as free_hours,
            TIMESTAMPDIFF(SECOND, start_date, end_date) / 3600 / 24 as free_days,
            DATE_FORMAT(CURDATE(), "%Y-%m-01 00:00:00") as start_of_month,
            LAST_DAY(CURDATE()) as end_of_month')ж*/
    }


    public function calculateFreeDaysForMonth($start, $end)
    {

        // Обчислюємо кількість вільних годин в періоді
        $freeHours = 0;

        foreach ($this->rc_bookings as $booking) {
            // Перевіряємо, чи бронювання знаходиться в періоді
            $bookingStart = Carbon::parse($booking->start_date);
            $bookingEnd = Carbon::parse($booking->end_date);

            if ($bookingStart >= $start && $bookingEnd <= $end) {
                // Визначаємо тривалість бронювання в годинах
                $durationHours = $bookingStart->diffInHours($bookingEnd);

                // Додаємо тривалість бронювання до загальної кількості годин
                $freeHours += $durationHours;
            }
        }

        // Обчислюємо кількість вільних днів
        $totalDays = $end->diffInDays($start) + 1; // +1 для включення обох кінців
        $freeDays = $totalDays - ($freeHours >= 9 * ($end->diffInDays($start) + 1) ? 1 : 0); // Перевірка на кількість вільних годин

        $this->total_days = $totalDays;
        $this->free_days = $freeDays;
    }

    public function calculateFreeDays($start, $end)
    {

        // Обчислюємо кількість вільних годин в періоді
        $freeHours = 0;

        foreach ($this->rc_bookings as $booking) {
            // Перевіряємо, чи бронювання знаходиться в періоді
            $bookingStart = Carbon::parse($booking->start_date);
            $bookingEnd = Carbon::parse($booking->end_date);

            if ($bookingStart >= $start && $bookingEnd <= $end) {
                // Визначаємо тривалість бронювання в годинах
                $durationHours = $bookingStart->diffInHours($bookingEnd);

                // Додаємо тривалість бронювання до загальної кількості годин
                $freeHours += $durationHours;
            }
        }

        // Обчислюємо кількість вільних днів
        $totalDays = $this->calculateDaysBetweenDates($start, $end);
        $freeDays = $totalDays - ($freeHours >= 9 * ($end->diffInDays($start) + 1) ? 1 : 0); // Перевірка на кількість вільних годин

        $this->total_days = $totalDays;
        $this->free_days = $freeDays;
    }

    function calculateDaysBetweenDates($start, $end)
    {
        $startDate = Carbon::parse($start);
        $endDate = Carbon::parse($end);

        // Обчислюємо кількість днів між датами
        $daysBetween = $endDate->diffInDays($startDate) + 1; // +1 для включення обох кінців

        return $daysBetween;
    }

    function calculateDays($bookings, $startDate, $endDate)
    {

        $totalOccupiedHours = 0;
        $totalOccupiedDays = 0;
        $FreeDays = 0;

        foreach ($bookings as $booking) {
            if($booking->start_date != null && $booking->end_date != null) {
                $bookingStart = max(Carbon::parse($booking->start_date), $startDate);
                $bookingEnd = min(Carbon::parse($booking->end_date), $endDate);
                $occupiedHours = $bookingStart->diffInHours($bookingEnd);
                $totalOccupiedHours += $occupiedHours;
            }

            //$booking->bookingStart = $bookingStart;
            /*if($occupiedHours > 9){
                $totalOccupiedDays++;
            }*/
        }

        // Перевірка, чи авто було вільним протягом 9 і більше годин
        $totalFreeHours = $startDate->diffInHours($endDate) - $totalOccupiedHours;
        $totalFreeDays = $startDate->diffInDays($endDate) - $totalOccupiedDays;


        $totalDays = $startDate->diffInDays($endDate) + 1;

        //return $totalDays;

        $this->total_days = $totalDays;
        $this->free_days = $totalOccupiedHours != $startDate->diffInHours($endDate) ? round($totalFreeHours / 24) : round($startDate->diffInHours($endDate) / 24);//round
        $this->free_days_2 = ($totalOccupiedHours);
    }
}
