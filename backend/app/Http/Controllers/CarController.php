<?php

namespace App\Http\Controllers;

use App\Http\Resources\CarBrandResource;
use App\Models\Bookings\RcBookings;
use App\Models\Cars\RcCars;
use App\Traits\LocalizationTrait;
use App\Traits\CarsBookingTrait;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CarController extends Controller
{
    use LocalizationTrait, CarsBookingTrait;

    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        try {
            $this->checkLocalization($request->lang);

            $filters = $request->filters;

            $currentDate = Carbon::now();
            $startDate = Carbon::now()->startOfMonth();
            $endDate = Carbon::now()->endOfMonth();

            $rc_carsQuery = RcCars::where([
                'status' => 1,
                'company_id' => 1,
                'is_deleted' => !1,
            ])->with('rc_cars_translations', 'rc_cars_models', 'rc_bookings');

            if (!empty($filters['model']) && !empty($filters['brand'])) {
                $rc_carsQuery->where('car_model_id', $filters['model']);
            }

            if (!empty($filters['brand'])) {
                $rc_carsQuery->whereHas('rc_cars_models.rc_cars_brands', function ($query) use ($filters) {
                    $query->where('car_brand_id', $filters['brand']);
                });
            }

            if (!empty($filters['start']) && !empty($filters['end'])) {
                $startDate = Carbon::parse($filters['start'])->startOfDay();
                $endDate = Carbon::parse($filters['end'])->endOfDay();
            }
            if (!empty($filters['year']) && !empty($filters['month'])) {
                $year = $filters['year'];
                $month = $filters['month'];

                $startDate = Carbon::create($year, $month, 1)->startOfMonth();
                $endDate = Carbon::create($year, $month, 1)->endOfMonth();
            }

            $rc_cars = $rc_carsQuery->paginate(30, ['*'], 'currentPage', $request->input('currentPage', 1));

            foreach ($rc_cars as $rc_car) {
                $bookings = RcBookings::where(function ($query) use ($startDate, $endDate) {
                    $query->whereBetween('start_date', [$startDate, $endDate])
                        ->orWhereBetween('end_date', [$startDate, $endDate])
                        ->orWhere(function ($query) use ($startDate, $endDate) {
                            $query->where('start_date', '<', $startDate)
                                ->where('end_date', '>', $endDate);
                        });
                })->where([
                    'status' => 1,
                    'company_id' => 1,
                    'is_deleted' => !1,
                    'car_id' => $rc_car->car_id,
                ])
                    ->whereColumn('start_date', '<', 'end_date')
                    ->orderBy('start_date')
                    ->get();

                $tt = $rc_car->calculateDays($bookings, $startDate, $endDate);
            }

            return response()->json([
                'rc_cars' => $rc_cars,
            ], 200);
        } catch
        (Exception $exception) {
            return response()->json([
                'message' => $exception->getMessage()
            ], 400);
        }
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(Request $request, $lang, $car_id)
    {
        try {
            $this->checkLocalization($lang);

            $rc_carsQuery = RcCars::where([
                'status' => 1,
                'company_id' => 1,
                'is_deleted' => !1,
                'car_id' => $car_id,
            ])->with('rc_cars_translations', 'rc_cars_models', 'rc_bookings');

            $rc_bookings = RcBookings::where([
                'status' => 1,
                'company_id' => 1,
                'is_deleted' => !1,
                'car_id' => $car_id,
            ])
                ->whereColumn('start_date', '<', 'end_date')
                ->orderBy('start_date');

            return response()->json([
                'lang' => $lang,
                'car_id' => $car_id,
                'rc_car' => $rc_carsQuery->first(),
                'rc_bookings' => $rc_bookings->get(),
            ], 200);
        } catch
        (Exception $exception) {
            return response()->json([
                'message' => $exception->getMessage()
            ], 400);
        }
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
