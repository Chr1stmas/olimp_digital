<?php

namespace App\Http\Controllers;

use App\Traits\LocalizationTrait;
use Exception;
use Illuminate\Http\Request;

class LocalizationController extends Controller
{
    use LocalizationTrait;

    public function GetLanguage($lang = 'en')
    {
        try {

            return response()->json([
                'lang' => $this->checkLocalization($lang)->original,
            ], 200);

        } catch (Exception $exception) {
            return response()->json([
                'message' => $exception->getMessage()
            ], 400);
        }
    }
}
