<?php

namespace App\Http\Controllers;

use App\Http\Resources\CarBrandResource;
use App\Models\Cars\RcCars;
use App\Models\CarsBrands\RcCarsBrands;
use App\Models\CarsBrands\RcCarsBrandsTranslation;
use App\Models\CarsModels\RcCarsModels;
use App\Traits\LocalizationTrait;
use Exception;
use Illuminate\Http\Request;

class CarBrandController extends Controller
{
    use LocalizationTrait;

    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        try {
            $this->checkLocalization($request->lang);

            $rc_cars_brands = RcCarsBrands::
            with('rc_cars_brands_translations')
                ->where([
                    'status' => 1,
                    'is_deleted' => 0,
                ]);

            $rc_cars_brands = $rc_cars_brands->get();

            return response()->json([
                'rc_cars_brands' => $rc_cars_brands,
                //'rc_cars_models' => $rc_cars_models,
            ], 200);

        } catch (Exception $exception) {
            return response()->json([
                'message' => $exception->getMessage()
            ], 400);
        }
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show($lang, string $car_brand_id)//RcCarsBrands $RcCarsBrands
    {
        try {
            $this->checkLocalization($lang);

            $rc_cars_models = RcCarsModels::with(['rc_cars_models_translations' => function ($query) {
                $query->where('lang', '=', app()->getLocale());
            }])->where('car_brand_id', $car_brand_id)->get();

            return response()->json([
                'rc_cars_models' => $rc_cars_models,
            ], 200);
        } catch (Exception $exception) {
            return response()->json([
                'message' => $exception->getMessage()
            ], 400);
        }
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
