<?php

namespace App\Http\Controllers;

use App\Models\Bookings\RcBookings;
use App\Models\Cars\RcCars;
use App\Traits\LocalizationTrait;
use Exception;
use Illuminate\Http\Request;

class BookingController extends Controller
{
    use LocalizationTrait;

    // Метод для перевірки доступності авто
    public function checkAvailability(Request $request)
    {
        try {
            $this->checkLocalization($request->lang);

            $rc_bookings = RcBookings::get();

            return response()->json([
                'lang' => $request->lang,
                'rc_bookings' => $rc_bookings,
            ], 200);
        } catch (Exception $exception) {
            return response()->json([
                'message' => $exception->getMessage()
            ], 400);
        }
    }
}
