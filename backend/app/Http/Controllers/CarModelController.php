<?php

namespace App\Http\Controllers;

use App\Models\CarsModels\RcCarsModels;
use App\Traits\LocalizationTrait;
use Exception;
use Illuminate\Http\Request;

class CarModelController extends Controller
{
    use LocalizationTrait;

    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        try {
            $this->checkLocalization($request->lang);

            $rc_cars_models = RcCarsModels::with('rc_cars_models_translations')->where([
                'status' => 1,
                'is_deleted' => !1,
                'car_brand_id' => $request->brand,
            ])->get();

            return response()->json([
                'brand' => $request->brand,
                'page' => $request->input('page', 1),
                'lang' => $request->lang,
                'rc_cars_models' => $rc_cars_models,
            ], 200);
        } catch (Exception $exception) {
            return response()->json([
                'message' => $exception->getMessage()
            ], 400);
        }
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
