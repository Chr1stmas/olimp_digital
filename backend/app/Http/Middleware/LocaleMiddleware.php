<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;
use Symfony\Component\HttpFoundation\Response;
use App\Models\CarsBrands\RcCarsBrandsTranslation;

class LocaleMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        $locale = $request->input('locale'); // Параметр "locale" в запиті

        //dd(LaravelLocalization::getCurrentLocale());
        if (!empty($locale) && in_array($locale, LaravelLocalization::getSupportedLocales())) {
            LaravelLocalization::setLocale($locale);
        } else {
            // Встановлюємо локаль за замовчуванням
            LaravelLocalization::setLocale(config('app.locale'));
        }

        return $next($request);


        ////V1
        /*// Отримати мову з параметра URL (наприклад, з першого сегмента)
        $locale = $request->segment(1);

        $locales = RcCarsBrandsTranslation::distinct()->pluck('lang');
        // Перевірити, чи підтримується ця мова у додатку
        if (!in_array($locale, $locales)) {
            $locale = 'en'; // Якщо мова не підтримується, встановити мову за замовчуванням
        }

        // Встановити обрану мову у додатку
        app()->setLocale($locale);

        return $next($request);*/
    }
}
