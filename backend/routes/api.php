<?php

use App\Http\Controllers\BookingController;
use App\Http\Controllers\CarBrandController;
use App\Http\Controllers\CarController;
use App\Http\Controllers\CarModelController;
use App\Http\Controllers\LocalizationController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;
use Mcamara\LaravelLocalization\Middleware\LaravelLocalizationRoutes;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::apiResource('{lang}/cars_brands', CarBrandController::class);
Route::apiResource('{lang}/cars_models', CarModelController::class);
Route::apiResource('{lang}/cars', CarController::class);
//Get Lang
Route::get('/{lang}/lang', [LocalizationController::class, 'GetLanguage']);
//Booking
Route::get('/{lang}/availability', [BookingController::class, 'checkAvailability']);

/*Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});*/
