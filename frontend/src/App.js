import React, {Suspense, useState} from "react";
import './App.css';
import Header from "./components/layouts/Header";
import {createBrowserRouter, Navigate, Route, Router, RouterProvider} from "react-router-dom";
import 'bootstrap/dist/css/bootstrap.css';
import CarsBrands from "./components/pages/CarsBrands";
import AllCarsAndFilters from "./components/all-cars-and-filters/AllCarsAndFilters";
import axios from 'axios';
import {I18nextProvider} from "react-i18next";
import i18n from "./http/i18n";
import Car from "./components/car-info/Car";


function App() {

    const [loading, setLoading] = useState(true);

    const router = createBrowserRouter([
        {
            path: "/",
            element: <Navigate to="/en/"/>,
        },
        {
            path: "/:lang/",
            element: <Header loading={loading} setLoading={setLoading}/>,
            children: [
                /*{
                    path: "/:lang/",
                    element: <CarsBrands loading={loading} setLoading={setLoading}/>,
                },*/
                /*{
                    path: "/:lang/cars",
                    element: <AllCarsAndFilters/>,
                },*/
                {
                    path: "/:lang/",
                    element: <AllCarsAndFilters/>,
                },
                {
                    path: "/:lang/car/:car_id",
                    element: <Car/>,
                },
                {
                    path: "*",
                    element: <h1>404</h1>,
                },
            ],
        },
    ]);

    // Set main base url
    axios.defaults.baseURL = "http://localhost:8000";
    axios.defaults.withCredentials = true;
    // Beare Token Save
    axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('token');

    return (
        <I18nextProvider i18n={i18n}>
            <Suspense>
                <RouterProvider router={router}/>
            </Suspense>
        </I18nextProvider>

        /*<I18nextProvider i18n={i18n}>
            <Router>
                <div>
                    <Route path="/" exact component={CarsBrands} />
                    {/!* Add other routes for different pages *!/}
                </div>
            </Router>
        </I18nextProvider>*/
    );
}

export default App;
