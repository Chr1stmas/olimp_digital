import Form from 'react-bootstrap/Form';
import Accordion from 'react-bootstrap/Accordion';
import Dropdown from 'react-bootstrap/Dropdown';
import {useState} from "react";
import Button from "react-bootstrap/Button";
import axiosClient from "../../http/axiosClient";
import {useParams} from "react-router-dom";
import Datetime from 'react-datetime';
import 'react-datetime/css/react-datetime.css';

function Filter({carsBrands, onFilterChange, carsModels, setCarsModels}) {
    const {lang} = useParams();
    const [filters, setFilters] = useState({
        brand: null,
        model: null,
        year: null,
        month: null,
        start: null,
        end: null,
    });
    const currentYear = new Date().getFullYear();
    const years = Array.from({length: 10}, (_, index) => currentYear - index); // Генеруємо список років, наприклад, останні 10 років
    const months = [
        {value: '1', label: 'January'},
        {value: '2', label: 'February'},
        {value: '3', label: 'March'},
        {value: '4', label: 'April'},
        {value: '5', label: 'May'},
        {value: '6', label: 'June'},
        {value: '7', label: 'July'},
        {value: '8', label: 'August'},
        {value: '9', label: 'September'},
        {value: '10', label: 'October'},
        {value: '11', label: 'November'},
        {value: '12', label: 'December'},
    ];
    const [periodType, setPeriodType] = useState('custom');

    const handlePeriodTypeChange = (e) => {
        setPeriodType(e.target.value);
        {
            periodType === 'custom' ?
                setFilters((prevFilters) => ({
                    ...prevFilters,
                    year: years[0],
                    month: months[0].value,
                }))
                : setFilters((prevFilters) => ({
                    ...prevFilters,
                    year: null,
                    month: null,
                }))
        }
    };

    const handleInputChange = (e) => {
        const {name, value} = e.target;
        setFilters((prevFilters) => ({
            ...prevFilters,
            model: name === 'brand' ? null : prevFilters.model,
            [name]: value,
        }));
        console.log('value');
        console.log(value);
    };

    const handleClear = (e) => {
        setFilters({
            brand: null,
            model: null,
            year: null,
            month: null,
            start: null,
            end: null,
        });
        setPeriodType('custom');
    };

    const models = (e) => {
        const {value} = e.target;
        axiosClient.get(`/api/${lang}/cars_models`, {
            params: {
                brand: value,
            },
        })//(`/api/${lang}/cars?page=${currentPage}`)
            .then(response => {
                setCarsModels(response.data.rc_cars_models);

            })
            .catch(function (message) {
                console.log('error');
                console.log(message);
            })
    };

    const applyFilters = () => {
        const currentFilters = {...filters};
        onFilterChange(currentFilters);
        console.log('filters', currentFilters);
    };

    /*const handleBrandSelect = (brand) => {
        setFilters(brand);
        onFilterChange(brand);
    };*/

    return (
        <Form>
            {/*<select value={periodType} onChange={handlePeriodTypeChange}>
                <option value="custom">Custom Date Range</option>
                <option value="month-year">Month/Year</option>
            </select>*/}
            <Form.Select onChange={handlePeriodTypeChange}
                         value={periodType}>
                <option value="custom">Custom Date Range</option>
                <option value="month-year">Month/Year</option>
            </Form.Select>

            {periodType === 'custom' && (
                <div>
                    <input type="date" value={filters.start} name='start' onChange={(e) => {
                        handleInputChange(e);
                    }}/>
                    <input type="date" value={filters.end} name='end' onChange={(e) => {
                        handleInputChange(e);
                    }}/>
                </div>
            )}

            {periodType === 'month-year' && (
                <div>
                    <br/>
                    <h6>Year</h6>
                    <Form.Select aria-label="Year" name='year' onChange={handleInputChange}
                                 defaultValue={years[0]}
                                 value={filters.year || years[0]}>
                        {/*<option value="">Year</option>*/}
                        {years.map((year, index) => (
                            <option
                                key={index}
                                value={year}
                            >{year}</option>
                        ))}
                    </Form.Select>
                    <br/>
                    <h6>Month</h6>
                    <Form.Select aria-label="Month" name='month' onChange={handleInputChange}
                                 defaultValue={months[0]}
                                 value={filters.month || months[0]}>
                        {/*<option value="">Month</option>*/}
                        {months.map((month, index) => (
                            <option
                                key={index}
                                value={month.value}
                            >{month.value}</option>
                        ))}
                    </Form.Select>
                </div>
            )}


            {/*<input type="date" value={filters.start} name='start' onChange={handleInputChange} />*/}
            {/*<Datetime
                dateFormat="YYYY" timeFormat={false}
                value={filters.datetime}
                onChange={(newDate) => {
                    handleInputChange({
                        target: {
                            name: 'startdatetime',
                            value: newDate
                        }
                    });
                }}
            />*/}
            {/*<br/>
            <Form.Select aria-label="Year" name='year' onChange={handleInputChange} value={filters.year || ''}>
                {years.map((year, index) => (
                    <option
                        key={index}
                        value={year}
                    >{year}</option>
                ))}
            </Form.Select>
            <br/>
            <Form.Select aria-label="Month" name='month' onChange={handleInputChange} value={filters.month || ''}>
                {months.map((month, index) => (
                    <option
                        key={index}
                        value={month.value}
                    >{month.value}</option>
                ))}
            </Form.Select>
            <br/>*/}
            <br/>
            <Dropdown>
                <Dropdown.Toggle variant="success" id="dropdown-basic">
                    Brands
                </Dropdown.Toggle>
                <Dropdown.Menu>
                    {/*<input
                        type="text"
                        name="brand"
                        placeholder="Марка"
                        onChange={handleInputChange}
                    />*/}
                    {carsBrands.map((brand) =>
                        <Dropdown.Item key={brand.car_brand_id}
                                       onClick={() => {
                                           handleInputChange({
                                               target: {
                                                   name: 'brand',
                                                   value: brand.car_brand_id
                                               }
                                           });
                                           models({target: {name: 'brand', value: brand.car_brand_id}});
                                       }}
                                       className={filters.brand === brand.car_brand_id ?
                                           'active'
                                           : null}
                        >
                            {brand.rc_cars_brands_translations.name}
                        </Dropdown.Item>
                    )}
                </Dropdown.Menu>
            </Dropdown>
            <br/>
            {
                filters.brand ?
                    <>
                        <Dropdown>
                            <Dropdown.Toggle variant="success" id="dropdown-basic">
                                Models
                            </Dropdown.Toggle>
                            <Dropdown.Menu>
                                {carsModels.map((model) =>
                                    <Dropdown.Item key={model.car_model_id}
                                                   onClick={() => {
                                                       handleInputChange({
                                                           target: {
                                                               name: 'model',
                                                               value: model.car_model_id
                                                           }
                                                       })
                                                   }}
                                                   className={filters.model === model.car_model_id ?
                                                       'active'
                                                       : null}
                                    >
                                        {model.rc_cars_models_translations.name}
                                    </Dropdown.Item>
                                )}
                            </Dropdown.Menu>
                        </Dropdown>
                        <br/>
                    </>
                    : null
            }
            <Button variant="primary" onClick={applyFilters}>Primary</Button>
            {filters.brand || filters.year || filters.start || filters.end ?
                <Button variant="danger" onClick={handleClear}>Clear</Button>
                : null
            }
            {/*{['checkbox', 'checkbox', 'checkbox'].map((type, index) => (
                <Accordion defaultActiveKey={`${index}`}>
                    <Accordion.Item eventKey={`${index}`}>
                        <Accordion.Header>Accordion Item #{`${index}`}</Accordion.Header>
                        <Accordion.Body>
                            {['checkbox', 'checkbox', 'checkbox'].map((type, index) => (
                                <div key={`default-checkbox`} className="mb-2">
                                    <Form.Check // prettier-ignore
                                        type='checkbox'
                                        id={`default-checkbox-${index}`}
                                        label={`default checkbox-${index}`}
                                    />
                                </div>
                            ))}
                        </Accordion.Body>
                    </Accordion.Item>
                </Accordion>
            ))}*/}
        </Form>
    );
}

export default Filter;