import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Filter from "./Filter";
import Cars from "./Cars";
import {useEffect, useState} from "react";
import axiosClient from "../../http/axiosClient";
import {useParams} from "react-router-dom";

function AllCarsAndFilters() {
    const {lang} = useParams();
    const [filters, setFilters] = useState({});
    const [carsBrands, setCarsBrands] = useState([]);
    const [carsModels, setCarsModels] = useState([]);
    const [currentPage, setCurrentPage] = useState(1);
    const [loading, setLoading] = useState(false);


    useEffect(() => {
        setLoading(false);
        axiosClient.get(`/api/${lang}/cars_brands`)
            .then(response => {
                console.log('success');
                console.log(response.data.rc_cars_brands);
                setCarsBrands(response.data.rc_cars_brands);
                setLoading(true);
            })
            .catch(function (message) {
                console.log('error');
                console.log(message);
                setLoading(true);
            })
    }, [lang, filters]);

    const handleFilterChange = (newFilters) => {
        setFilters(newFilters);
        setCurrentPage(1)
    };

    const handlePageChange = (newPage) => {
        setCurrentPage(newPage);
    };

    return (
        <Container fluid>
            <Row>
                <Col sm={4} md="auto">
                    <h6 align='center'>Filter</h6>
                    <Filter onFilterChange={handleFilterChange} carsBrands={carsBrands} carsModels={carsModels} setCarsModels={setCarsModels}/>
                </Col>
                <Col>
                    <h6 align='center'>Cars</h6>
                    <Cars filters={filters} currentPage={currentPage} onPageChange={handlePageChange} loading={loading} setLoading={setLoading} />
                </Col>
            </Row>
        </Container>
    );
}

export default AllCarsAndFilters;