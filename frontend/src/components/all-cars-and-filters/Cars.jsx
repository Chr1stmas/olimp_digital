import Card from 'react-bootstrap/Card';
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import MyPagination from "../pagination/Pagination";
import {Spinner, Table} from "react-bootstrap";
import {useEffect, useState} from "react";
import axiosClient from "../../http/axiosClient";
import {Link, useParams} from "react-router-dom";
import ReactPaginate from "react-paginate";
import Container from "react-bootstrap/Container";
import {useTranslation} from "react-i18next";

function Cars({filters, currentPage, onPageChange, loading, setLoading}) {
    const {lang} = useParams();
    const {t, i18n} = useTranslation();
    const [cars, setCars] = useState([]);
    const [data, setData] = useState([]);

    useEffect(() => {
        setLoading(false);

        i18n.changeLanguage(lang);
        axiosClient.get(`/api/${lang}/cars`, {
            params: {
                currentPage,
                filters,
            },
        })//(`/api/${lang}/cars?page=${currentPage}`)
            .then(response => {
                console.log('success');
                console.log(response.data);
                console.log(response.data.rc_cars.last_page);
                setData(response.data.rc_cars);
                setCars(response.data.rc_cars.data);
                setLoading(true);
                //console.log(response.data.rc_cars.data);
            })
            .catch(function (message) {
                console.log('error');
                console.log(message);
                setLoading(true);
            })
    }, [lang, filters, currentPage]);

    // Invoke when user click to request another page.
    const handlePageClick = (newPage) => {
        const selectedPage = newPage.selected + 1;
        onPageChange(selectedPage);
    };

    return (
        <>
            {loading ?
                <Table striped bordered hover>
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>{t('title')}</th>
                        <th>0-100</th>
                        <th>{t('free_days')}</th>
                        <th>{t('total_days')}</th>
                    </tr>
                    </thead>
                    {cars.map((c) =>
                        <tbody>
                        <tr>
                            <td>{c.car_id}</td>
                            <td>
                                <Link to={`/${lang}/car/${c.car_id}`}>
                                    {c.rc_cars_translations.title}
                                </Link>
                            </td>
                            <td>{c.rc_cars_models.attribute_0_100}</td>
                            <td>{c.free_days}</td>
                            <td>{c.total_days}</td>
                        </tr>
                        </tbody>
                    )}
                </Table>
                :
                <Container fluid="sm" className="d-flex justify-content-center align-items-center">
                    <Spinner animation="border" role="status">
                        <span className="visually-hidden">Loading...</span>
                    </Spinner>
                </Container>
            }
            <Row className="justify-content-md-center mt-4">
                <Col md="auto">
                    <ReactPaginate
                        nextLabel="next >"
                        onPageChange={handlePageClick}
                        pageRangeDisplayed={5}
                        marginPagesDisplayed={2}
                        pageCount={data.last_page}
                        previousLabel="< previous"
                        pageClassName="page-item"
                        pageLinkClassName="page-link"
                        previousClassName="page-item"
                        previousLinkClassName="page-link"
                        nextClassName="page-item"
                        nextLinkClassName="page-link"
                        breakLabel="..."
                        breakClassName="page-item"
                        breakLinkClassName="page-link"
                        containerClassName="pagination"
                        activeClassName="active"
                        renderOnZeroPageCount={null}
                    />
                </Col>
            </Row>
        </>
    );
}

export default Cars;