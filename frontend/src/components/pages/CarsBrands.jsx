import Card from 'react-bootstrap/Card';
import Container from "react-bootstrap/Container";
import {useEffect, useState} from "react";
import axiosClient from "../../http/axiosClient";
import {Link, useParams} from "react-router-dom";
import axios from 'axios';
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import {CardGroup, Spinner} from "react-bootstrap";

function CarsBrands(props) {
    const {lang} = useParams();
    const [brands, setBrands] = useState([]);

    useEffect(() => {
        axiosClient.get(`/api/${lang}/cars_brands`)
            .then(response => {
                console.log('success');
                console.log(response.data.rc_cars_brands);
                setBrands(response.data.rc_cars_brands);
            })
            .catch(function (message) {
                console.log('error');
                console.log(message);
            })
    }, [lang]);

    return (
        props.loading ?
            <Container fluid="sm">
                <Row xs={1} md={5} className="g-4">
                    {
                        brands.map((brand) => (
                            <Col key={brand.car_brand_id}>
                                <Card>
                                    <Card.Img variant="top" src="holder.js/100px160"/>
                                    <Card.Body>
                                        <Card.Title>{brand.rc_cars_brands_translations
                                            ? brand.rc_cars_brands_translations.name
                                            : null}</Card.Title>
                                        <Card.Text>
                                            This is a longer card with supporting text below as a natural
                                            lead-in to additional content. This content is a little bit
                                            longer.
                                        </Card.Text>
                                        <Card.Link><Link>Card Link</Link></Card.Link>
                                    </Card.Body>
                                </Card>
                            </Col>
                        ))
                    }
                </Row>
            </Container>
            :
            <Container fluid="sm" className="d-flex justify-content-center align-items-center">
                <Spinner animation="border" role="status">
                    <span className="visually-hidden">Loading...</span>
                </Spinner>
            </Container>
    );
}

export default CarsBrands;