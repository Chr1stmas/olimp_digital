import {useEffect, useState} from "react";
import axiosClient from "../../http/axiosClient";
import {Link, useParams} from "react-router-dom";
import {useTranslation} from "react-i18next";
import {Spinner, Table} from "react-bootstrap";
import Container from "react-bootstrap/Container";

function Car() {
    const {t, i18n} = useTranslation();
    const {lang, car_id} = useParams();
    const [loading, setLoading] = useState(false);
    const [bookings, setBookings] = useState([]);
    const [data, setData] = useState([]);

    useEffect(() => {
        setLoading(false);

        i18n.changeLanguage(lang);
        axiosClient.get(`/api/${lang}/cars/${car_id}`, {
            params: {car_id: car_id}
        })
            .then(response => {
                console.log('success');
                console.log(response.data);
                setBookings(response.data.rc_bookings);
                setData(response.data.rc_car);
                setLoading(true);
            })
            .catch(function (message) {
                console.log('error');
                console.log(message);
                setLoading(true);
            })
    }, [lang]);

    return (
        <Container fluid>
            {loading ?
                <Table striped bordered hover>
                    <thead>
                    <tr>
                        <th>car_id</th>
                        <th>booking_id</th>
                        <th>start_date</th>
                        <th>end_date</th>
                    </tr>
                    </thead>
                    {bookings.map((booking) =>
                        <tbody>
                        <tr>
                            <td>{booking.car_id}</td>
                            <td>{booking.booking_id}</td>
                            <td>{booking.start_date}</td>
                            <td>{booking.end_date}</td>
                        </tr>
                        </tbody>
                    )}
                </Table>
                :
                <Container fluid="sm" className="d-flex justify-content-center align-items-center">
                    <Spinner animation="border" role="status">
                        <span className="visually-hidden">Loading...</span>
                    </Spinner>
                </Container>
            }
        </Container>
    );
}

export default Car;