import * as React from 'react';
import {Outlet} from "react-router-dom";
import OffcanvasExample from "./Navbar";

export default function Header(props) {

    return (
        <>
            <OffcanvasExample loading={props.loading} setLoading={props.setLoading}/>
            <Outlet/>
        </>
    );
}