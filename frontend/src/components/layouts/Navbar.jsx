import Button from 'react-bootstrap/Button';
import Container from 'react-bootstrap/Container';
import Form from 'react-bootstrap/Form';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import NavDropdown from 'react-bootstrap/NavDropdown';
import Offcanvas from 'react-bootstrap/Offcanvas';
import {Link, useParams, useNavigate, useLocation} from "react-router-dom";
import {useTranslation} from "react-i18next";
import {useEffect, useState} from "react";
import axiosClient from "../../http/axiosClient";

function OffcanvasExample(props) {
    const {lang} = useParams();
    const {t, i18n} = useTranslation();
    const [locales, setLocales] = useState([]);
    const location = useLocation();
    const navigate = useNavigate();

    const changeLanguage = (language) => {
        i18n.changeLanguage(language);

        const newPath = location.pathname.replace(/\/[a-z]{2}(\/|$)/, `/${language}$1`);

        navigate(newPath);
    };

    useEffect(() => {
        props.setLoading(false);
        axiosClient.get(`/api/${lang}/lang`)
            .then(response => {
                setLocales(response.data.lang.locales);
                console.log('response.data.lang.locales\n');
                console.log(response.data.lang.locales);
                props.setLoading(true);
            })
            .catch(function (message) {
                console.log('error');
                console.log(message.response.data.message);
                props.setLoading(true);
            })
    }, [lang, i18n]);

    return (
        <>
            {[false].map((expand) => (
                <Navbar key={expand} expand={expand} className="bg-body-tertiary mb-3">
                    <Container fluid>
                        <Navbar.Brand href="#">{t('welcome')}</Navbar.Brand>
                        <Navbar.Toggle aria-controls={`offcanvasNavbar-expand-${expand}`}/>
                        <Navbar.Offcanvas
                            id={`offcanvasNavbar-expand-${expand}`}
                            aria-labelledby={`offcanvasNavbarLabel-expand-${expand}`}
                            placement="end"
                        >
                            <Offcanvas.Header closeButton>
                                <Offcanvas.Title id={`offcanvasNavbarLabel-expand-${expand}`}>
                                    {t('menu')}
                                </Offcanvas.Title>
                            </Offcanvas.Header>
                            <Offcanvas.Body>
                                <Nav className="justify-content-end flex-grow-1 pe-3">
                                    <Nav.Link>
                                        <Link to='/en'>All Cars</Link>
                                    </Nav.Link>
                                    <NavDropdown
                                        title="Dropdown"
                                        id={`offcanvasNavbarDropdown-expand-${expand}`}
                                    >
                                        {
                                            locales.map((l, index) => (
                                                <button key={index} className={l === lang ? 'active' : null}
                                                                  onClick={() => changeLanguage(l)}>{ l }</button>
                                            ))
                                        }
                                    </NavDropdown>
                                </Nav>
                                <Form className="d-flex">
                                    <Form.Control
                                        type="search"
                                        placeholder="Search"
                                        className="me-2"
                                        aria-label="Search"
                                    />
                                    <Button variant="outline-success">Search</Button>
                                </Form>
                            </Offcanvas.Body>
                        </Navbar.Offcanvas>
                    </Container>
                </Navbar>
            ))}
        </>
    );
}

export default OffcanvasExample;