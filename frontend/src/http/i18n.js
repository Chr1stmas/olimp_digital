import i18n from "i18next";
import { initReactI18next } from "react-i18next";
import ae from "../locales/ae.json";
import cn from "../locales/cn.json";
import de from "../locales/de.json";
import en from "../locales/en.json";
import es from "../locales/es.json";
import fr from "../locales/fr.json";
import it from "../locales/it.json";
import ru from "../locales/ru.json";

i18n
    .use(initReactI18next)
    .init({
        resources: {
            ae: {
                translation: ae,
            },
            cn: {
                translation: cn,
            },
            de: {
                translation: de,
            },
            en: {
                translation: en,
            },
            es: {
                translation: es,
            },
            fr: {
                translation: fr,
            },
            it: {
                translation: it,
            },
            ru: {
                translation: ru,
            },
        },
        lng: "en",
        fallbackLng: "en",
        interpolation: {
            escapeValue: false,
        },
    });

export default i18n;