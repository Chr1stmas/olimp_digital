import axios from 'axios';

const axiosClient = axios.create({
    baseURL: "http://localhost:8000"
});

axiosClient.defaults.withCredentials = true;

export default axiosClient;